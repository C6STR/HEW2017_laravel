window.onload=function(){
//初期化(あまりにもひどいのであとで書き直す)
  div_radio_color('hair_color_box','hair_color_box05');
  div_radio_color('head_color_box','head_color_box01');
  div_radio_color('face_color_box','face_color_box02');
  div_radio_color('outer_color_box','outer_color_box02');
  div_radio_color('inner_color_box','inner_color_box07');
  div_radio_color('bottom_color_box','bottom_color_box02');
  div_radio_color('shoes_color_box','shoes_color_box01');
  div_radio_color('other_color_box','other_color_box04');
  div_radio('hair_1','hair_box');
  div_radio('head_1','head_box');
  div_radio('face_1','face_box');
  div_radio('outer_1','outer_box');
  div_radio('inner_1','inner_box');
  div_radio('bottom_1','bottom_box');
  div_radio('shoes_1','shoes_box');
  div_radio('shoes_1','shoes_box');
  draw();
}
function draw(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);
    draw_other();
    draw_hair_ico();
    draw_head_ico();
    draw_face_ico();
    draw_outer_ico();
    draw_inner_ico();
    draw_bottom_ico();
    draw_shoes_ico();
    draw_other_ico();
};

function draw_hair(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image();
  var element_s = document.getElementById( "hair_select" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_s = element_s.hair ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_s=radioNodeList_s.value;

  var element_c = document.getElementById( "hair_color" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_c = element_c.colora ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_c=radioNodeList_c.value;
  var pass = pass_s + pass_c;
  img.addEventListener("load", function() {
    ctx.drawImage(img,0,0,400,400);
    draw_inner();
  }, false);
  img.src=pass;
};

function draw_head(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image();
  var element_s = document.getElementById( "head_select" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_s = element_s.head ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_s=radioNodeList_s.value;

  var element_c = document.getElementById( "head_color" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_c = element_c.colora ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_c=radioNodeList_c.value;
  var pass = pass_s + pass_c;
  img.addEventListener("load", function() {
    ctx.drawImage(img,0,0,400,400);
    draw_face();
  }, false);
  img.src=pass;
};

function draw_body(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image();
  var pass_s="./image/chara/body_01_";

  var element_c = document.getElementById("head_color");
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_c = element_c.colora ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_c=radioNodeList_c.value;
  var pass = pass_s + pass_c;
  img.addEventListener("load", function() {
    ctx.drawImage(img,0,0,400,400);
    draw_head();
  }, false);
  img.src=pass;
};

function draw_face(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image();
  var element_s = document.getElementById( "face_select" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_s = element_s.facea ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_s=radioNodeList_s.value;

  var element_c = document.getElementById( "face_color" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_c = element_c.colora ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_c=radioNodeList_c.value;
  var pass = pass_s + pass_c;
  img.addEventListener("load", function() {
    ctx.drawImage(img,0,0,400,400);
    draw_hair();
  }, false);
  img.src=pass;
};

function draw_outer(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image();
  var element_s = document.getElementById( "outer_select" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_s = element_s.outer ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_s=radioNodeList_s.value;

  var element_c = document.getElementById( "outer_color" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_c = element_c.colora ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_c=radioNodeList_c.value;
  var pass = pass_s + pass_c;
  img.addEventListener("load", function() {
    ctx.drawImage(img,0,0,400,400);
    draw_shoes();
  }, false);
  img.src=pass;
};

function draw_inner(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image();
  var element_s = document.getElementById( "inner_select" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_s = element_s.inner ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_s=radioNodeList_s.value;

  var element_c = document.getElementById( "inner_color" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_c = element_c.colora ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_c=radioNodeList_c.value;
  var pass = pass_s + pass_c;
  img.addEventListener("load", function() {
    ctx.drawImage(img,0,0,400,400);
    draw_bottom();
  }, false);
  img.src=pass;
};

function draw_bottom(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image();
  var element_s = document.getElementById( "bottom_select" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_s = element_s.bottoma ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_s=radioNodeList_s.value;

  var element_c = document.getElementById( "bottom_color" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_c = element_c.colora ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_c=radioNodeList_c.value;
  var pass = pass_s + pass_c;
  img.addEventListener("load", function() {
    ctx.drawImage(img,0,0,400,400);
    draw_outer();
  }, false);
  img.src=pass;
};

function draw_shoes(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image();
  var element_s = document.getElementById( "shoes_select" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_s = element_s.shoes ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_s=radioNodeList_s.value;

  var element_c = document.getElementById( "shoes_color" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_c = element_c.colora ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_c=radioNodeList_c.value;
  var pass = pass_s + pass_c;
  img.addEventListener("load", function() {
    ctx.drawImage(img,0,0,400,400);
  }, false);
  img.src=pass;
};

function draw_other(){
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image();
  var element_s = document.getElementById( "other_select" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_s = element_s.other ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_s=radioNodeList_s.value;

  var element_c = document.getElementById( "other_color" ) ;
  // form要素内のラジオボタングループ(name="hoge")を取得
  var radioNodeList_c = element_c.colora ;
  // 選択状態の値(value)を取得 (Bが選択状態なら"b"が返る)
  var pass_c=radioNodeList_c.value;
  var pass = pass_s + pass_c;
  img.addEventListener("load", function() {
    ctx.drawImage(img,0,0,400,400);
    draw_body();
  }, false);
  img.src=pass;
};
function ChangeTab(tabname) {
  document.getElementById('tab1').style.display = 'none';
  document.getElementById('tab2').style.display = 'none';
  document.getElementById('tab3').style.display = 'none';
  document.getElementById('tab4').style.display = 'none';
  document.getElementById('tab5').style.display = 'none';
  document.getElementById('tab6').style.display = 'none';
  document.getElementById('tab7').style.display = 'none';
  document.getElementById('tab8').style.display = 'none';
  document.getElementById(tabname).style.display = 'block';
};
function ChangeColorTab(tabname) {
  document.getElementById('color_tab1').style.display = 'none';
  document.getElementById('color_tab2').style.display = 'none';
  document.getElementById('color_tab3').style.display = 'none';
  document.getElementById('color_tab4').style.display = 'none';
  document.getElementById('color_tab5').style.display = 'none';
  document.getElementById('color_tab6').style.display = 'none';
  document.getElementById('color_tab7').style.display = 'none';
  document.getElementById('color_tab8').style.display = 'none';
  document.getElementById(tabname).style.display = 'block';
};

function saveCanvas(saveType){
  var imageType = "image/png";
  var fileName = "cchara.png";
  if(saveType === "jpeg"){
      imageType = "image/jpeg";
      fileName = "cchara.jpg";
  }
  var canvas = document.getElementById("canvas");
  // base64エンコードされたデータを取得 「data:image/png;base64,iVBORw0k～」
  var base64 = canvas.toDataURL(imageType);
  // base64データをblobに変換
  var blob = Base64toBlob(base64);
  // blobデータをa要素を使ってダウンロード
  saveBlob(blob, fileName);
}

//画像ほぞんするやつ
function Base64toBlob(base64)
{
  // カンマで分割して以下のようにデータを分ける
  // tmp[0] : データ形式（data:image/png;base64）
  // tmp[1] : base64データ（iVBORw0k～）
  var tmp = base64.split(',');
  // base64データの文字列をデコード
  var data = atob(tmp[1]);
  // tmp[0]の文字列（data:image/png;base64）からコンテンツタイプ（image/png）部分を取得
var mime = tmp[0].split(':')[1].split(';')[0];
  //  1文字ごとにUTF-16コードを表す 0から65535 の整数を取得
var buf = new Uint8Array(data.length);
for (var i = 0; i < data.length; i++) {
      buf[i] = data.charCodeAt(i);
  }
  // blobデータを作成
var blob = new Blob([buf], { type: mime });
  return blob;
}

// 画像のダウンロード
function saveBlob(blob, fileName)
{
  var url = (window.URL || window.webkitURL);
  // ダウンロード用のURL作成
  var dataUrl = url.createObjectURL(blob);
  // イベント作成
  var event = document.createEvent("MouseEvents");
  event.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
  // a要素を作成
  var a = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
  // ダウンロード用のURLセット
  a.href = dataUrl;
  // ファイル名セット
  a.download = fileName;
  // イベントの発火
  a.dispatchEvent(event);
}




//あいこんかえるとこ
function draw_hair_ico(){
for(i=1;i<=11;i++){
  var num=i;
  var num_padding=( '00' + num ).slice( -2 );
  var element = document.getElementById( "hair_color" ) ;
  var radioNodeList_s = element.colora ;
  var bg_color=radioNodeList_s.value;
  var img="./image/chara/hair_" + num_padding + "_" + bg_color;
  var bg = document.getElementById("hair_"+i);
  //bg.style.backgroundColor="#BBCCDD";
  bg.style.backgroundImage="url("+img+")";
};
};
function draw_head_ico(){
for(i=1;i<=4;i++){
  var num=i;
  var num_padding=( '00' + num ).slice( -2 );
  var element = document.getElementById( "head_color" ) ;
  var radioNodeList_s = element.colora ;
  var bg_color=radioNodeList_s.value;
  var img="./image/chara/head_" + num_padding + "_" + bg_color;
  var bg = document.getElementById("head_"+i);
  //bg.style.backgroundColor="#BBCCDD";
  bg.style.backgroundImage="url("+img+")";
};
};
function draw_face_ico(){
for(i=1;i<=8;i++){
  var num=i;
  var num_padding=( '00' + num ).slice( -2 );
  var element = document.getElementById( "face_color" ) ;
  var radioNodeList_s = element.colora ;
  var bg_color=radioNodeList_s.value;
  var img="./image/chara/face_" + num_padding + "_" + bg_color;
  var bg = document.getElementById("face_"+i);
  //bg.style.backgroundColor="#BBCCDD";
  bg.style.backgroundImage="url("+img+")";
};
};
function draw_outer_ico(){
for(i=1;i<=4;i++){
  var num=i;
  var num_padding=( '00' + num ).slice( -2 );
  var element = document.getElementById( "outer_color" ) ;
  var radioNodeList_s = element.colora ;
  var bg_color=radioNodeList_s.value;
  var img="./image/chara/outer_" + num_padding + "_" + bg_color;
  var bg = document.getElementById("outer_"+i);
  //bg.style.backgroundColor="#BBCCDD";
  bg.style.backgroundImage="url("+img+")";
};
};
function draw_inner_ico(){
for(i=1;i<=2;i++){
  var num=i;
  var num_padding=( '00' + num ).slice( -2 );
  var element = document.getElementById( "inner_color" ) ;
  var radioNodeList_s = element.colora ;
  var bg_color=radioNodeList_s.value;
  var img="./image/chara/shirt_" + num_padding + "_" + bg_color;
  var bg = document.getElementById("inner_"+i);
  //bg.style.backgroundColor="#BBCCDD";
  bg.style.backgroundImage="url("+img+")";
};
};
function draw_bottom_ico(){
for(i=1;i<=3;i++){
  var num=i;
  var num_padding=( '00' + num ).slice( -2 );
  var element = document.getElementById( "bottom_color" ) ;
  var radioNodeList_s = element.colora ;
  var bg_color=radioNodeList_s.value;
  var img="./image/chara/bottom_" + num_padding + "_" + bg_color;
  var bg = document.getElementById("bottom_"+i);
  //bg.style.backgroundColor="#BBCCDD";
  bg.style.backgroundImage="url("+img+")";
};
};
function draw_shoes_ico(){
for(i=1;i<=2;i++){
  var num=i;
  var num_padding=( '00' + num ).slice( -2 );
  var element = document.getElementById( "shoes_color" ) ;
  var radioNodeList_s = element.colora ;
  var bg_color=radioNodeList_s.value;
  var img="./image/chara/shoes_" + num_padding + "_" + bg_color;
  var bg = document.getElementById("shoes_"+i);
  //bg.style.backgroundColor="#BBCCDD";
  bg.style.backgroundImage="url("+img+")";
};
};
function draw_other_ico(){
for(i=1;i<=3;i++){
  var num=i;
  var num_padding=( '00' + num ).slice( -2 );
  var element = document.getElementById( "other_color" ) ;
  var radioNodeList_s = element.colora ;
  var bg_color=radioNodeList_s.value;
  var img="./image/bg/bg_" + num_padding + "_" + bg_color;
  var bg = document.getElementById("other_"+i);
  bg.style.backgroundImage="url("+img+")";
};
};

function div_radio(target,parts){
  draw();
  //alert(target);
  select_change_color_default(target,parts);
  var element = document.getElementById(target);
  element.checked = true;
//何故かなしでも動く
};
//選択状態
function select_change_color_default(target,parts){
  $(function(){
    $("." + parts).css("border-color","rgba(8,8,8,0.2)");
  });
  select_change_color(target);
}
function select_change_color(target){
  $(function select_change_color(){
    $("#" + target).css("border-color","rgba(255,0,0,0.75)");
  });
}

function div_radio_color(parts,target){
  draw();
  select_color_box_default(parts,target)
}

function select_color_box_default(parts,target){
  $(function(){
    $("." + parts).css("border-color","rgba(8,8,8,0.2)");
  });
  select_color_box(target);
}
function select_color_box(target){
  $(function select_change_color(){
    $("." + target).css("border-color","rgba(255,0,0,0.75)");
  });
}

//ここから追加する
$(function(){
  $(".button_hover").hover(
    function(){
      $(this).css("filter","brightness(80%)");
    },
    function(){
      $(this).css("filter","brightness(100%)");
    }
  );
});

//cookieに保存
function save_cookie(){
  var cookie_data_select = [
    document.getElementById("hair_select"),
    document.getElementById("head_select"),
    document.getElementById("face_select"),
    document.getElementById("outer_select"),
    document.getElementById("inner_select"),
    document.getElementById("bottom_select"),
    document.getElementById("shoes_select"),
    document.getElementById("other_select"),
  ];
  var cookie_data_color = [
    document.getElementById("hair_color"),
    document.getElementById("head_color"),
    document.getElementById("face_color"),
    document.getElementById("outer_color"),
    document.getElementById("inner_color"),
    document.getElementById("bottom_color"),
    document.getElementById("shoes_color"),
    document.getElementById("other_color"),
  ];
}
